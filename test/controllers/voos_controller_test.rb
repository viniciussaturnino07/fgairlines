require 'test_helper'

class VoosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @voo = voos(:one)
  end

  test "should get index" do
    get voos_url
    assert_response :success
  end

  test "should get new" do
    get new_voo_url
    assert_response :success
  end

  test "should create voo" do
    assert_difference('Voo.count') do
      post voos_url, params: { voo: { codigo: @voo.codigo, data: @voo.data, destino: @voo.destino, empresa: @voo.empresa, preco: @voo.preco } }
    end

    assert_redirected_to voo_url(Voo.last)
  end

  test "should show voo" do
    get voo_url(@voo)
    assert_response :success
  end

  test "should get edit" do
    get edit_voo_url(@voo)
    assert_response :success
  end

  test "should update voo" do
    patch voo_url(@voo), params: { voo: { codigo: @voo.codigo, data: @voo.data, destino: @voo.destino, empresa: @voo.empresa, preco: @voo.preco } }
    assert_redirected_to voo_url(@voo)
  end

  test "should destroy voo" do
    assert_difference('Voo.count', -1) do
      delete voo_url(@voo)
    end

    assert_redirected_to voos_url
  end
end
