require "application_system_test_case"

class VoosTest < ApplicationSystemTestCase
  setup do
    @voo = voos(:one)
  end

  test "visiting the index" do
    visit voos_url
    assert_selector "h1", text: "Voos"
  end

  test "creating a Voo" do
    visit voos_url
    click_on "New Voo"

    fill_in "Codigo", with: @voo.codigo
    fill_in "Data", with: @voo.data
    fill_in "Destino", with: @voo.destino
    fill_in "Empresa", with: @voo.empresa
    fill_in "Preco", with: @voo.preco
    click_on "Create Voo"

    assert_text "Voo was successfully created"
    click_on "Back"
  end

  test "updating a Voo" do
    visit voos_url
    click_on "Edit", match: :first

    fill_in "Codigo", with: @voo.codigo
    fill_in "Data", with: @voo.data
    fill_in "Destino", with: @voo.destino
    fill_in "Empresa", with: @voo.empresa
    fill_in "Preco", with: @voo.preco
    click_on "Update Voo"

    assert_text "Voo was successfully updated"
    click_on "Back"
  end

  test "destroying a Voo" do
    visit voos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Voo was successfully destroyed"
  end
end
