class CreateVoos < ActiveRecord::Migration[6.0]
  def change
    create_table :voos do |t|
      t.string :codigo
      t.string :destino
      t.string :data
      t.decimal :preco
      t.string :empresa

      t.timestamps
    end
  end
end
