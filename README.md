# FGAirlines

# Sobre o projeto

**Nome:** Vinicius de Sousa Saturnino  
**Matrícula:** 18/0132245  
**Nome:** Mateus Gomes do Nascimento  
**Matrícula:** 18/0106821  

Este projeto foi desenvolvido para a disciplina de Orientação a Objetos que tinha como objetivo desenvolver uma aplicação web em linguagem Ruby com a framework Rails.

## Requisitos para execução do projeto

Ruby version 2.6.5 p114  
Rails 6.0.1


## Para rodar o programa

Abra o seu terminal e dê o comando git clone "link desse repositório"
entre na pasta do projeto e execute as seguintes linhas no terminal:

1. bundle install
2. yarn install --check-files
3. rails db:migrate
4. rails s

* Após executar esses comandos entre no seu navegador e acesse o endereço: localhost:3000

# Sobre o projeto

O objetivo principal do desenvolvimento dessa aplicação web, é criar um sitema que possua funcionalidades que permitam a reserva em um voo específico pela parte do usuário. E pela parte do administrador, é possível cadastrar novos administradores, além de adicionar, remover e editar voos.

## Funcionalidades

#### A aplicação possui as seguintes funcionalidades:

- Login, Logout, Sign Up
- Visualização de usuários e voos (ambos)
- Edição de usuario e voo (Somente o adm)
- Cadastro de novos voos (Somente o adm)
- Reserva de voo (ambos)
- Cadastro de novos adm's (Somente o adm)
- Página de comprovação de reserva

## Gem's utilizadas

1. gem devise (autenticação e cadastro de usuários)
2. gem cancancan (Gerenciamento de permissões de usuário)
3. gem bootstrap (Front-end do website)
