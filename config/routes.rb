Rails.application.routes.draw do
  get 'homepage/index'
  get 'confirmation/index'
  root to: redirect('/homepage/index')
  devise_for :users
  resources :voos
  resources :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
